<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetGiftOne()
    {
        // echo json_encode($perid);
        // $encodePassword = md5($password);
        $sql = "SELECT REGISTER_ID, PERID, NAME, SURNAME, Dep_name, GIFT FROM viewgift";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        return $jsonData;
    }
}
