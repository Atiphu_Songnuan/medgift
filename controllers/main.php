<?php
class Main extends Controller
{
    public function __construct()
    {
        parent::__construct('main');
        $this->views->GetGiftOne = $this->model->GetGiftOne();
    }
    

    public function index()
    {
        $this->views->render('main/index');
    }

}
