<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>MedGift</title>
    
    <link rel="icon" type="image/png" href="./public/images/icons/gift.ico"/> 

    <link href="./public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="./public/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <!-- SB Admin 2 -->
    <link href="./public/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="./public/vendor/sweetalert2/dist/sweetalert2.min.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
    <style>
        body {
            background-image: linear-gradient(-225deg, #E3FDF5 0%, #FFE6FA 100%);
            font-family: 'Kanit', serif;
            font-size: 28px;
        }
        label{
            font-size: 18px;
        }
        h4{
            font-size: 24px;
        }
        h3{
            font-size: 28px;
        }
        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }
        div.dataTables_info , a{
            font-size: 14px;
        }
        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            vertical-align: middle;
            width: 1%;
        }

        .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        td{
            white-space:nowrap;
            font-size: 14px;
            /* vertical-align: middle; */
        }
        tr{
            font-family: 'Kanit', serif;
            font-size: 16px;
        }

        .edit-modal .modal {
            position: relative;
            top: auto;
            bottom: auto;
            right: auto;
            left: auto;
            display: block;
            z-index: 1;
        }
        .edit-modal .modal {
            background: transparent !important;
        }
        .swal2-popup {
		font-size: 0.7rem !important;
	    }

        .navbar-center
        {
            position: absolute;
            width: 100%;
            left: 0;
            top: 0;
            text-align: center;
            overflow: visible;
            height: 0;
        }
    </style>
</head>
<body class="wrapper">

    <!-- Require Header from header.php -->
    <?php require './views/header.php'?>

    <div class="container-fluid">
        <!-- Page Heading -->
        <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h4 class="mb-0 text-success">รางวัลเงินสดมูลค่า 10,000 บาท <br><small><span class="text-danger">(จำนวน 1 รางวัล)</span></small></h4>
        </div> -->

        <!-- Card 1 -->
        <div class="row">
            <div class="col-xl-11 col-md-12 mb-4 mx-auto">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h4 class="mb-0 text-success">รางวัลเงินสดมูลค่า 10,000 บาท <br><small><span class="text-danger">(จำนวน 1 รางวัล)</span></small></h4>
                </div>
                <div class="card border-left-success shadow py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="giftonetable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="bg bg-primary text-white">
                                            <th>ID</th>
                                            <th>รหัสพนักงาน</th>
                                            <th>ชื่อ-นามสกุล</th>
                                            <th>หน่วยงาน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $giftoneJsonDecode = json_decode($this->GetGiftOne,true);
                                            for ($i=0; $i < count($giftoneJsonDecode); $i++) {
                                    
                                            //     $personid = $personweightJsonDecode[$i]['PERID'];
                                                if ($giftoneJsonDecode[$i]['GIFT'] == '10,000 บาท') {
                                                    echo '<tr>';
                                                        echo '<td>';
                                                            if ($giftoneJsonDecode[$i]['REGISTER_ID'] != "") {
                                                                echo $giftoneJsonDecode[$i]['REGISTER_ID'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td class="text-danger" align="center" style="font-size: 22px">';
                                                            echo '<span class="badge badge-danger">';
                                                                if ($giftoneJsonDecode[$i]['PERID'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['PERID'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
    
                                                        echo '<td class="text-info" style="font-size: 18px">';
                                                            if ($giftoneJsonDecode[$i]['NAME'] != "" && $giftoneJsonDecode[$i]['SURNAME'] != "") {
                                                                echo $giftoneJsonDecode[$i]['NAME']. " " . $giftoneJsonDecode[$i]['SURNAME'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td style="font-size: 22px">';
                                                            echo '<span class="badge badge-success">';
                                                                if ($giftoneJsonDecode[$i]['Dep_name'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['Dep_name'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
                                                    echo '</tr>';
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Card 1 --> 
        </div>

        <!-- Card 2 -->
        <div class="row">
            <div class="col-xl-11 col-md-12 mb-4 mx-auto">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h4 class="mb-0 text-success">รางวัลเงินสดมูลค่า 5,000 บาท <br><small><span class="text-danger">(จำนวน 5 รางวัล)</span></small></h4>
                </div>
                <div class="card border-left-success shadow py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="gifttwotable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="bg bg-primary text-white">
                                            <th>ID</th>
                                            <th>รหัสพนักงาน</th>
                                            <th>ชื่อ-นามสกุล</th>
                                            <th>หน่วยงาน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $giftoneJsonDecode = json_decode($this->GetGiftOne,true);
                                            for ($i=0; $i < count($giftoneJsonDecode); $i++) {
                                    
                                            //     $personid = $personweightJsonDecode[$i]['PERID'];
                                                if ($giftoneJsonDecode[$i]['GIFT'] == '5,000 บาท') {
                                                    echo '<tr>';
                                                        echo '<td>';
                                                            if ($giftoneJsonDecode[$i]['REGISTER_ID'] != "") {
                                                                echo $giftoneJsonDecode[$i]['REGISTER_ID'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td class="text-danger" align="center" style="font-size: 22px">';
                                                            echo '<span class="badge badge-danger">';
                                                                if ($giftoneJsonDecode[$i]['PERID'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['PERID'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
    
                                                        echo '<td class="text-info" style="font-size: 18px">';
                                                            if ($giftoneJsonDecode[$i]['NAME'] != "" && $giftoneJsonDecode[$i]['SURNAME'] != "") {
                                                                echo $giftoneJsonDecode[$i]['NAME']. " " . $giftoneJsonDecode[$i]['SURNAME'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td style="font-size: 22px">';
                                                            echo '<span class="badge badge-success">';
                                                                if ($giftoneJsonDecode[$i]['Dep_name'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['Dep_name'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
                                                    echo '</tr>';
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Card 2 --> 
        </div>

        <!-- Card 2 -->
        <div class="row">
            <div class="col-xl-11 col-md-12 mb-4 mx-auto">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h4 class="mb-0 text-success">รางวัลเงินสดมูลค่า 2,000 บาท <br><small><span class="text-danger">(จำนวน 30 รางวัล)</span></small></h4>
                </div>
                <div class="card border-left-success shadow py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="giftthreetable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="bg bg-primary text-white">
                                            <th>ID</th>
                                            <th>รหัสพนักงาน</th>
                                            <th>ชื่อ-นามสกุล</th>
                                            <th>หน่วยงาน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $giftoneJsonDecode = json_decode($this->GetGiftOne,true);
                                            for ($i=0; $i < count($giftoneJsonDecode); $i++) {
                                    
                                            //     $personid = $personweightJsonDecode[$i]['PERID'];
                                                if ($giftoneJsonDecode[$i]['GIFT'] == '2,000 บาท') {
                                                    echo '<tr>';
                                                        echo '<td>';
                                                            if ($giftoneJsonDecode[$i]['REGISTER_ID'] != "") {
                                                                echo $giftoneJsonDecode[$i]['REGISTER_ID'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td class="text-danger" align="center" style="font-size: 22px">';
                                                            echo '<span class="badge badge-danger">';
                                                                if ($giftoneJsonDecode[$i]['PERID'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['PERID'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
    
                                                        echo '<td class="text-info" style="font-size: 18px">';
                                                            if ($giftoneJsonDecode[$i]['NAME'] != "" && $giftoneJsonDecode[$i]['SURNAME'] != "") {
                                                                echo $giftoneJsonDecode[$i]['NAME']. " " . $giftoneJsonDecode[$i]['SURNAME'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td style="font-size: 22px">';
                                                            echo '<span class="badge badge-success">';
                                                                if ($giftoneJsonDecode[$i]['Dep_name'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['Dep_name'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
                                                    echo '</tr>';
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Card 2 --> 
        </div>

        <!-- Card 3 -->
        <div class="row">
            <div class="col-xl-11 col-md-12 mb-4 mx-auto">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h4 class="mb-0 text-success">รางวัลเงินสดมูลค่า 1,000 บาท <br><small><span class="text-danger">(จำนวน 75 รางวัล)</span></small></h4>
                </div>
                <div class="card border-left-success shadow py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="giftfourtable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="bg bg-primary text-white">
                                            <th>ID</th>
                                            <th>รหัสพนักงาน</th>
                                            <th>ชื่อ-นามสกุล</th>
                                            <th>หน่วยงาน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $giftoneJsonDecode = json_decode($this->GetGiftOne,true);
                                            for ($i=0; $i < count($giftoneJsonDecode); $i++) {
                                    
                                            //     $personid = $personweightJsonDecode[$i]['PERID'];
                                                if ($giftoneJsonDecode[$i]['GIFT'] == '1,000 บาท') {
                                                    echo '<tr>';
                                                        echo '<td>';
                                                            if ($giftoneJsonDecode[$i]['REGISTER_ID'] != "") {
                                                                echo $giftoneJsonDecode[$i]['REGISTER_ID'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td class="text-danger" align="center" style="font-size: 22px">';
                                                            echo '<span class="badge badge-danger">';
                                                                if ($giftoneJsonDecode[$i]['PERID'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['PERID'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
    
                                                        echo '<td class="text-info" style="font-size: 18px">';
                                                            if ($giftoneJsonDecode[$i]['NAME'] != "" && $giftoneJsonDecode[$i]['SURNAME'] != "") {
                                                                echo $giftoneJsonDecode[$i]['NAME']. " " . $giftoneJsonDecode[$i]['SURNAME'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td style="font-size: 22px">';
                                                            echo '<span class="badge badge-success">';
                                                                if ($giftoneJsonDecode[$i]['Dep_name'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['Dep_name'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
                                                    echo '</tr>';
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Card 3 --> 
        </div>

        <!-- Card 3 -->
        <div class="row">
            <div class="col-xl-11 col-md-12 mb-4 mx-auto">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h4 class="mb-0 text-success">รางวัลเงินสดมูลค่า 500 บาท <br><small><span class="text-danger">(จำนวน 359 รางวัล)</span></small></h4>
                </div>
                <div class="card border-left-success shadow py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="giftfivetable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="bg bg-primary text-white">
                                            <th>ID</th>
                                            <th>รหัสพนักงาน</th>
                                            <th>ชื่อ-นามสกุล</th>
                                            <th>หน่วยงาน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $giftoneJsonDecode = json_decode($this->GetGiftOne,true);
                                            for ($i=0; $i < count($giftoneJsonDecode); $i++) {
                                    
                                            //     $personid = $personweightJsonDecode[$i]['PERID'];
                                                if ($giftoneJsonDecode[$i]['GIFT'] == '500 บาท') {
                                                    echo '<tr>';
                                                        echo '<td>';
                                                            if ($giftoneJsonDecode[$i]['REGISTER_ID'] != "") {
                                                                echo $giftoneJsonDecode[$i]['REGISTER_ID'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td class="text-danger" align="center" style="font-size: 22px">';
                                                            echo '<span class="badge badge-danger">';
                                                                if ($giftoneJsonDecode[$i]['PERID'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['PERID'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
    
                                                        echo '<td class="text-info" style="font-size: 18px">';
                                                            if ($giftoneJsonDecode[$i]['NAME'] != "" && $giftoneJsonDecode[$i]['SURNAME'] != "") {
                                                                echo $giftoneJsonDecode[$i]['NAME']. " " . $giftoneJsonDecode[$i]['SURNAME'];
                                                            } else{
                                                                echo "-";
                                                            }
                                                        echo '</td>';
    
                                                        echo '<td style="font-size: 22px">';
                                                            echo '<span class="badge badge-success">';
                                                                if ($giftoneJsonDecode[$i]['Dep_name'] != "") {
                                                                    echo $giftoneJsonDecode[$i]['Dep_name'];
                                                                } else{
                                                                    echo "-";
                                                                }
                                                            echo '</span>';
                                                        echo '</td>';
                                                    echo '</tr>';
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Card 3 --> 
        </div>
    </div>

   <!-- Bootstrap core JavaScript-->
   <script src="./public/vendor/jquery/jquery.min.js"></script>
    <script src="./public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Datatable -->
    <script src="./public/vendor/datatables/jquery.dataTables.js"></script>
    <script src="./public/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Demo scripts for this page-->
    <script src="./public/js/demo/datatables-demo.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="./public/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="./public/js/sb-admin-2.min.js"></script>

    <!-- SweetAlert2 -->
    <script src="./public/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

    <script>
        $(function(){
            $('#giftonetable').dataTable({
                "order": [[ 0, "desc" ]],
                "aLengthMenu": [1],
                "iDisplayLength": 1,
                // "bInfo": false,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
                        "searchable": false,
                    }, 
                ]
            });

            $('#gifttwotable').dataTable({
                "order": [[ 0, "desc" ]],
                "aLengthMenu": [5],
                "iDisplayLength": 5,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
                        "searchable": false
                    }
                ]
            });

            $('#giftthreetable').dataTable({
                "order": [[ 0, "desc" ]],
                "aLengthMenu": [10],
                "iDisplayLength": 10,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
                        "searchable": false
                    }
                ]
            });

            $('#giftfourtable').dataTable({
                "order": [[ 0, "desc" ]],
                "aLengthMenu": [30],
                "iDisplayLength": 30,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
                        "searchable": false
                    }
                ]
            });

            $('#giftfivetable').dataTable({
                "order": [[ 0, "desc" ]],
                "aLengthMenu": [30],
                "iDisplayLength": 30,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
                        "searchable": false
                    }
                ]
            });
        });     
    </script>
</body>
</html>