<header class="main-header">

    <!-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow"> -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow" style="background-image: linear-gradient(-225deg, #E3FDF5 0%, #FFE6FA 100%);">
        <!-- Sidebar Toggle (Topbar) -->
        <h3 class="navbar-center text-primary mt-3">ผลการจับรางวัล</h3>  
    </nav>
</header>

<script>
    function Logout(){
        Swal.fire({
        title: 'ต้องการออกจากระบบหรือไม่?',
        // text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ใช่',
        cancelButtonText: 'ยกเลิก'
        }).then((result) => {
        if (result.value) {
            $.ajax({
            type:"POST",
            url:"../medweight/ApiService/SessionDestroy",
            success: function() {
                Swal.fire({
                title: 'ออกจากระบบสำเร็จ',
                text: '',
                type: 'success',
                timer: 2000,
                showConfirmButton: false,
                onClose: () =>{
                    window.location.href='../medweight/login';
                }
                });
            },
            error: function() {
                console.log('Error occured');
            }
            });
        }
        });
    }
</script>